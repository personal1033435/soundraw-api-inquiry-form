import React, { useEffect, useRef } from 'react';
import SwaggerUI from 'swagger-ui-react';
import 'swagger-ui-react/swagger-ui.css';
import '../../src/swagger-custom.css';

const SwaggerComponent: React.FC = () => {
  const swaggerRef = useRef(null);
  useEffect(() => {
    const h4Elements = document.querySelectorAll('h4');

    h4Elements.forEach((h4) => {
      console.log("H4s", h4)
      if (h4.innerText === 'Server response') {
        h4.style.color = 'red';
      }
    });
  }, [swaggerRef]);
  return (
    <>
      <SwaggerUI url="/swagger.json" ref={swaggerRef}/>
    </>
  );
}

export default SwaggerComponent