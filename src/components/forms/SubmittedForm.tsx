import React, { useState } from 'react';

const SubmittedForm: React.FC = () => {
  return (
    <div className='text-center md:py-[8em] py-[10em]'>
      <span className='text-white md:text-lg text-sm'>
       Thank you! We'll be in touch soon.
      </span>
      <div className="flex justify-center mt-[8em]">
        <button
          onClick={() => window.location.reload()}
          className="bg-gradient-to-r from-[#29ABE2] via-[#8E98DF] via-[#8E98DF] via-25% via-[#CDA9E8] via-50% via-[#F2BBB7] via-75% to-[#FFFB93] hover:bg-white text-white text-[1em] py-1 px-1 rounded-3xl"
        >
          <div className="bg-[#040B13] rounded-3xl p-2 pr-4 pl-4">
            <span className="tracking-[0.1em] hover:bg-gradient-to-r hover:from-[#29ABE2] hover:via-[#8E98DF] hover:via-[#8E98DF] hover:via-25% hover:via-[#CDA9E8] hover:via-50% hover:via-[#F2BBB7] hover:via-75% hover:to-[#FFFB93] hover:inline-block hover:text-transparent hover:bg-clip-text">
              TAKE ANOTHER SPIN!
            </span>
          </div>
        </button>
      </div>
    </div>
  )
}

export default SubmittedForm;