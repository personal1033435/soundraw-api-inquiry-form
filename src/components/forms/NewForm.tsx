import React, { useEffect, useState } from 'react';

interface NewFormProps {
  toggleSubmission: (val: boolean) => void;
  formData: {
    "entry.855893838": string,
    "entry.1027739922": string,
    "entry.935203910": string,
    "entry.1650490612": string,
    "entry.1122565078": string,
    "entry.1258150668": string,
    "entry.2099555532": string
  },
  setFormData: (val: object) => void;
}

const NewForm: React.FC<NewFormProps> = ({toggleSubmission, formData, setFormData}) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setFormData((prevData: any) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const [didSubmit, setDidSubmit] = useState(false);

  const handleSubmit = (e: React.FormEvent) => {
    setDidSubmit(true);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      if (didSubmit) {
        toggleSubmission(true);
      } else {
      }
    }, 1000);
    return () => clearTimeout(timer);
  }, [didSubmit]);

  return (
    <>
      <div className="mt-10 flex justify-center items-center">
        <form
          action="https://docs.google.com/forms/d/e/1FAIpQLSeC7ngSYjKrOrkNcelIMUU_HT9H8W1n7bdkrzxV_TiGjGBN9w/formResponse"
          className="md:w-3/4 md:p-10"
          target='responseFrame'
          onSubmit={handleSubmit}
        >
          <div className='grid md:grid-cols-2 border rounded-lg md:p-4 p-2'>
            <div className="m-4">
              <label htmlFor="name" className="block md:text-[1em] text-sm font-medium text-white mb-2">
                Your name <label className='text-[#ff0000] text-xs'>*</label>
              </label>
              <input
                type="text"
                id="name"
                name="entry.1122565078"
                className="w-full text-xs px-3 py-2 border rounded shadow appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                placeholder="Your name"
                value={formData["entry.1122565078"]}
                required
                onChange={handleChange}
              />
            </div>
            <div className="m-4">
              <label htmlFor="emailAdd" className="block md:text-[1em] text-sm font-medium text-white mb-2">
              Email address <label className='text-[#ff0000] text-xs'>*</label>
              </label>
              <input
                type="email"
                id="emailAdd"
                name="entry.1258150668"
                className="w-full text-xs px-3 py-2 border rounded shadow appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                placeholder="Email address"
                value={formData["entry.1258150668"]}
                required
                onChange={handleChange}
              />
            </div>
          </div>
          <div className='border rounded-lg md:p-4 p-2 mt-5'>
            <div className='grid md:grid-cols-2'>
              <div className="m-4">
                <label htmlFor="companyName" className="block md:text-[1em] text-sm font-medium text-white mb-2">
                  Company's name <label className='text-[#ff0000] text-xs'>*</label>
                </label>
                <input
                  type="text"
                  id="companyName"
                  name="entry.855893838"
                  className="w-full text-xs px-3 py-2 border rounded shadow appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  placeholder="Company's name"
                  value={formData["entry.855893838"]}
                  required
                  onChange={handleChange}
                />
              </div>
              <div className="m-4">
                <label htmlFor="website" className="block md:text-[1em] text-sm font-medium text-white mb-2">
                  Company's website <label className='text-[#ff0000] text-xs'>*</label>
                </label>
                <input
                  type="text"
                  id="website"
                  name="entry.1650490612"
                  className="w-full text-xs px-3 py-2 border rounded shadow appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  placeholder="Company's website"
                  value={formData["entry.1650490612"]}
                  required
                  onChange={handleChange}
                />
              </div>
              <div className="m-4">
                <label htmlFor="headquarters" className="block md:text-[1em] text-sm font-medium text-white mb-2">
                  Headquarters (Country) <label className='text-[#ff0000] text-xs'>*</label>
                </label>
                <input
                  type="text"
                  id="headquarters"
                  name="entry.1027739922"
                  className="w-full text-xs px-3 py-2 border rounded shadow appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  placeholder="Company's headquarters"
                  value={formData["entry.1027739922"]}
                  required
                  onChange={handleChange}
                />
              </div>
              <div className="m-4">
                <label htmlFor="mau" className="block md:text-[1em] text-sm font-medium text-white mb-2">
                  Monthly Active Users (MAU) <label className='text-[#ff0000] text-xs'>*</label>
                </label>
                <select
                  id="mau"
                  name="entry.935203910"
                  className="w-full text-xs px-3 py-2 border rounded shadow appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  value={formData["entry.935203910"]}
                  onChange={handleChange}
                >
                  <option value="0">0</option>
                  <option value="1 - 1,000">1 - 1,000</option>
                  <option value="1,001 - 10,000">1,001 - 10,000</option>
                  <option value="10,001 - 100,000">10,001 - 100,000</option>
                  <option value="100,001 - 1,000,000">100,001 - 1,000,000</option>
                  <option value="1,000,000 +">1,000,000 +</option>
                </select>
              </div>
            </div>
            <div className="m-4">
                <label htmlFor="useFor" className="block md:text-[1em] text-sm font-medium text-white mb-2">
                  How will you use SOUNDRAW's music? <label className='text-[#ff0000] text-xs'>*</label>
                </label>
                <input
                  type="text"
                  id="useFor"
                  name="entry.2099555532"
                  className="w-full text-xs px-3 py-2 border rounded shadow appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  placeholder="Enter your answer"
                  value={formData["entry.2099555532"]}
                  required
                  onChange={handleChange}
                />
              </div>
          </div>
          
          <div className="flex justify-center mt-10">
            <button
              type="submit"
              className="bg-gradient-to-r from-[#29ABE2] via-[#8E98DF] via-[#8E98DF] via-25% via-[#CDA9E8] via-50% via-[#F2BBB7] via-75% to-[#FFFB93] hover:bg-white text-white text-[1em] py-1 px-1 rounded-3xl"
            >
              <div className="bg-[#040B13] rounded-3xl p-2 pr-4 pl-4">
                <span className="tracking-[0.1em] hover:bg-gradient-to-r hover:from-[#29ABE2] hover:via-[#8E98DF] hover:via-[#8E98DF] hover:via-25% hover:via-[#CDA9E8] hover:via-50% hover:via-[#F2BBB7] hover:via-75% hover:to-[#FFFB93] hover:inline-block hover:text-transparent hover:bg-clip-text">
                  ROCK ON!
                </span>
              </div>
            </button>
          </div>
        </form>
      </div>

      <iframe
        name="responseFrame"
        title="Google Form Response"
        className="hidden"
      ></iframe>
    </>
  );
};

export default NewForm;
