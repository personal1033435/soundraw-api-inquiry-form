import React, { useState } from 'react';
import NewForm from './NewForm';
import SubmittedForm from './SubmittedForm';

const FormComponent: React.FC = () => {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const emptyFormData = {
    "entry.855893838": '',
    "entry.1027739922": '',
    "entry.935203910": '',
    "entry.1650490612": '',
    "entry.1122565078": '',
    "entry.1258150668": '',
    "entry.2099555532": ''
  }
  const [formData, setFormData] = useState(emptyFormData);

  const toggleSubmission = (val: boolean) => {
    setIsSubmitted(val)
  }

  const toggleFormData = (val: any) => {
    setFormData(val);
    setIsSubmitted(false);
  }

  const [headerDetails, setHeaderDetails] = useState([
    "POWERFUL AI MUSIC TOOL FOR DEVELOPERS",
    "SOUNDRAW API",
    "Thank you for reaching out! We're currently receiving a high volume of requests, so our response may be slightly delayed. Please know that we are committed to getting back to you as soon as possible. We appreciate your patience and understanding while we work through the queue. You'll hear from us very soon!"
  ])
  
  return (
    <div className="p-[3em]">
      <div className="min-h-full border-md md:m-10 text-center">
        <span className="mt-5 text-md-sm md:text-[0.85em] text-[0.7em] tracking-[0.2em] font-medium bg-gradient-to-r from-[#29ABE2] via-[#8E98DF] via-25% via-[#CDA9E8] via-50% via-[#F2BBB7] via-75% to-[#FFFB93] inline-block text-transparent bg-clip-text">
          { headerDetails[0] }
        </span>
        <h1 className="mb-5 text-white font-bold md:text-[4rem] text-[2em]">
          { headerDetails[1] }
        </h1>
        {!isSubmitted &&
          <>
            <span className="md:pl-10 md:pr-10 md:text-[1em] text-[0.75em] tracking-[0.1em] font-medium bg-gradient-to-r from-[#29ABE2] via-[#8E98DF] via-[#8E98DF] via-25% via-[#CDA9E8] via-50% via-[#F2BBB7] via-75% to-[#FFFB93] inline-block text-transparent bg-clip-text">
              { headerDetails[2] }
            </span>

            <div className="pt-[4em] md:pl-10 md:pr-10 md:text-[1em] text-[0.75em] tracking-[0.1em] font-medium flex justify-center">
              <div className="text-white bg-gradient-to-r from-[#29ABE2] via-[#8E98DF] via-[#8E98DF] via-25% via-[#CDA9E8] via-50% via-[#F2BBB7] via-75% to-[#FFFB93] inline-block text-transparent bg-clip-text">
                <span className="pr-[0.4em]">
                  ALREADY HAVE A TEST TOKEN?
                </span>
                <a href='/api' className="underline hover:bg-gradient-to-r hover:from-[#29ABE2] hover:via-[#8E98DF] hover:via-[#8E98DF] hover:via-25% via-[#CDA9E8] hover:via-50% hover:via-[#F2BBB7] hover:via-75% hover:to-[#FFFB93] hover:inline-block hover:text-transparent hover:bg-clip-text">
                  TEST THE API HERE.
                </a>
              </div>
            </div>

          </>
        }
      </div>
      <div className={`${isSubmitted ? 'hidden' : 'block'}`}>
        <NewForm toggleSubmission={toggleSubmission} formData={formData} setFormData={toggleFormData}/>
      </div>
      <div className={`${isSubmitted ? 'block' : 'hidden'}`}>
        <SubmittedForm/>
      </div>
    </div>
  )
}

export default FormComponent;