import React, { useEffect, useRef } from 'react';
import './App.css';
import FormComponent from './components/forms/FormComponent';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import SwaggerComponent from './components/SwaggerComponent';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/api" element={<SwaggerComponent />}/>
          <Route path="/" element={<FormComponent />} />
          <Route path="*" element={<Navigate to="/" />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
