/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
  extend: {
    fontFamily: {
      inter: ['Inter', 'sans'], // 'sans' is the generic font family
    },
    animation: {
      'spin': 'spin 2s linear infinite', // Customize animation duration
    },
  },
}

